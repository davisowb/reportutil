# encoding: utf-8

import os
import attr
import logging_helper
from time import sleep
from collections import OrderedDict
from reportutil import Report, ReportNodeTypes, ReportConstant, ReportingError
from reportutil._config import register_report_config
from tableutil import Table, HEADING

logging = logging_helper.setup_logging()


@attr.s
class ReportUtilTestData(object):
    test1 = attr.ib(default=1)
    test2 = attr.ib(default=2)
    test3 = attr.ib(default=3)
    random = attr.ib(default=456)
    test4 = attr.ib(default=4)
    test5 = attr.ib(default=5)


test_data = ReportUtilTestData()

cfg = register_report_config()

r = Report(root_dir=os.path.join(cfg.data_path_unversioned,
                                 ReportConstant.default_root_dir_name))

dev_test_node = r.add_child(name=u'ReportUtil Dev Testing',
                            node_class=ReportNodeTypes.dir)

dev_test_node_2 = r.add_child(name=u'ReportUtil Dev Testing 2',
                              node_class=ReportNodeTypes.dir)

dummy_node = dev_test_node.add_child(name=u'Dummy',
                                     node_class=ReportNodeTypes.dir)

dummy_file_node = dummy_node.add_child(name=u'Dummy File',
                                       node_class=ReportNodeTypes.html)

dummy_file_node.add(Table(headings=[{HEADING: col}
                                    for col in
                                    attr.asdict(test_data, dict_factory=OrderedDict).keys()]))

r.generate()

sleep(10)

dummy_node.move(location=dev_test_node_2)

sleep(10)

try:
    # This is expected to fail
    rerr = Report(root_dir=os.path.join(cfg.data_path_unversioned,
                                      u'Reports_2'))

except ReportingError as err:
    logging.error(err)

r.move(os.path.join(cfg.data_path_unversioned,
                         u'Reports_2'))


r2 = Report(root_dir=os.path.join(cfg.data_path_unversioned,
                                  u'Reports_2'))

sleep(10)

r2.move(os.path.join(cfg.data_path_unversioned,
                     ReportConstant.default_root_dir_name))  # Return report to its original location
